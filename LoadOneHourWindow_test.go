package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOneHourWindow(t *testing.T) {
	shopTrx := []Transaction{
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 5,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:01"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 4,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:02"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 3,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:03"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 33,
			EventTime: DateTime{
				parseTime("2019-12-01 01:00:01"),
			},
		},
	}

	expected := []Transaction{
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 5,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:01"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 4,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:02"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 3,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:03"),
			},
		},
	}
	actual := LoadOneHourWindow(shopTrx)
	assert.Equal(t, expected, actual)
}
