package main

func LoadUniqueShop(trx []*Transaction) (shops []int) {
	result := map[int]int{}
	for _, val := range trx {
		if _, ok := result[val.ShopID]; !ok {
			shops = append(shops, val.ShopID)
			result[val.ShopID] = val.ShopID
		}
	}
	return
}
