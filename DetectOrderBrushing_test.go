package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDetectOrderBrushingNo(t *testing.T) {
	oneHourWindow := []Transaction{
		Transaction{
			ShopID:  1,
			UserID:  1,
			OrderID: 5,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:01"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 4,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:02"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  3,
			OrderID: 3,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:03"),
			},
		},
	}
	expected := 0
	actual := DetectOrderBrushing(oneHourWindow)
	assert.Equal(t, expected, actual)
	return
}

func TestDetectOrderBrushingYes(t *testing.T) {
	oneHourWindow := []Transaction{
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 5,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:01"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 4,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:02"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 3,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:03"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  3,
			OrderID: 5,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:01"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  3,
			OrderID: 4,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:02"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  3,
			OrderID: 3,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:03"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  3,
			OrderID: 33,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:03"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  3,
			OrderID: 5,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:01"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  3,
			OrderID: 4,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:02"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  3,
			OrderID: 3,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:03"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  3,
			OrderID: 33,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:03"),
			},
		},
	}
	expected := 3
	actual := DetectOrderBrushing(oneHourWindow)
	assert.Equal(t, expected, actual)
	return
}
