package main

import (
	"fmt"
	"log"
	"os"
	"sort"
	"strings"

	"github.com/gocarina/gocsv"
)

type Brushing struct {
	ShopID  int    `csv:"shopid"`
	UserIDs string `csv:"userid"`
}

func main() {
	var b []Brushing
	log.Println("Loading Data")
	trx := LoadData()
	shops := LoadUniqueShop(trx)

	suspectedUserByShop := map[int][]int{}
	for index, shop := range shops {
		log.Println("Processing shop ", shop, ", shop no ", index)
		suspectedUserByShop[shop] = []int{}
		existedSuspectedUser := map[int]int{}
		trxByShop := LoadShopTransaction(shop, trx)
		tempTrxByShop := trxByShop
		for range trxByShop {
			oneHourWindow := LoadOneHourWindow(tempTrxByShop)
			suspectedUser := DetectOrderBrushing(oneHourWindow)
			if suspectedUser != 0 {
				if _, ok := existedSuspectedUser[suspectedUser]; !ok {
					suspectedUserByShop[shop] = append(suspectedUserByShop[shop], suspectedUser)
					existedSuspectedUser[suspectedUser] = suspectedUser
				}
			}
			tempTrxByShop = tempTrxByShop[1:]
		}
	}
	for shopID, suspectedUserIDs := range suspectedUserByShop {
		var row Brushing
		if len(suspectedUserIDs) > 0 {
			sortedSuspectedUserIds := suspectedUserIDs
			sort.Sort(IntSort(sortedSuspectedUserIds))
			var uids []string
			for _, suspectedUserID := range sortedSuspectedUserIds {
				uids = append(uids, fmt.Sprintf("%d", suspectedUserID))
			}
			row.UserIDs = strings.Join(uids, "&")
		} else {
			row.UserIDs = "0"
		}
		row.ShopID = shopID
		b = append(b, row)

		clientsFile, _ := os.OpenFile("order_suspected.csv", os.O_RDWR|os.O_CREATE, os.ModePerm)
		gocsv.MarshalFile(&b, clientsFile)

	}

}
