package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadDataTransaction(t *testing.T) {
	var data []*Transaction
	data = LoadData()
	assert.NotEmpty(t, data)
	if len(data) > 0 {
		// orderid,shopid,userid,event_time
		// 31076582227611,93950878,30530270,2019-12-27 00:23:03->
		assert.Equal(t, 31076582227611, data[0].OrderID)
		assert.Equal(t, 93950878, data[0].ShopID)
		assert.Equal(t, 30530270, data[0].UserID)
		assert.Equal(t, "2019-12-27 00:23:03", data[0].EventTime.Format("2006-01-02 15:04:05"))
	}
}
