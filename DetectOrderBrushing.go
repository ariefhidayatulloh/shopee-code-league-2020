package main

func DetectOrderBrushing(trx []Transaction) (result int) {
	var totalOrder, totalUser, maxOrderPerUser int
	totalOrder = len(trx)
	uniqueUser := map[int]int{}

	for _, val := range trx {
		if _, ok := uniqueUser[val.UserID]; !ok {
			totalUser += val.UserID
			uniqueUser[val.UserID] = 1
		} else {
			uniqueUser[val.UserID] += 1
		}

		if maxOrderPerUser < uniqueUser[val.UserID] {
			maxOrderPerUser = uniqueUser[val.UserID]
			result = val.UserID
		}

	}
	totalUser = len(uniqueUser)

	concreteOrder := totalOrder / totalUser
	if concreteOrder < 3 {
		result = 0
	}
	return
}
