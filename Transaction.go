package main

import "time"

// orderid,shopid,userid,event_time
type Transaction struct {
	OrderID   int      `csv:"orderid"`
	ShopID    int      `csv:"shopid"`
	UserID    int      `csv:"userid"`
	EventTime DateTime `csv:"event_time"`
}

type DateTime struct {
	time.Time
}

// Convert the internal date as CSV string 2019-12-27 00:23:03
func (date *DateTime) MarshalCSV() (string, error) {
	return date.Time.Format("2006-01-02 15:04:05"), nil
}

// You could also use the standard Stringer interface
func (date *DateTime) String() string {
	return date.String() // Redundant, just for example
}

// Convert the CSV string as internal date
func (date *DateTime) UnmarshalCSV(csv string) (err error) {
	date.Time, err = time.Parse("2006-01-02 15:04:05", csv)
	return err
}
