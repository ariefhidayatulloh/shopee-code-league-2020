package main

import "sort"

type ByTime []Transaction

func (a ByTime) Len() int           { return len(a) }
func (a ByTime) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByTime) Less(i, j int) bool { return a[i].EventTime.Unix() < a[j].EventTime.Unix() }

type IntSort []int

func (a IntSort) Len() int           { return len(a) }
func (a IntSort) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a IntSort) Less(i, j int) bool { return a[i] < a[j] }

func LoadShopTransaction(shopID int, trx []*Transaction) (result []Transaction) {

	for _, val := range trx {
		if shopID == val.ShopID {
			result = append(result, *val)
		}
	}
	sort.Sort(ByTime(result))
	return
}
