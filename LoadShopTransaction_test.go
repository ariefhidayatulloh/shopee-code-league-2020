package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadShopTransaction(t *testing.T) {
	trx := []*Transaction{
		&Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 3,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:03"),
			},
		},
		&Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 4,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:02"),
			},
		},
		&Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 5,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:01"),
			},
		},
		&Transaction{
			ShopID:  2,
			UserID:  22,
			OrderID: 32,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:01"),
			},
		},
	}
	expected := []Transaction{
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 5,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:01"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 4,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:02"),
			},
		},
		Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 3,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:03"),
			},
		},
	}
	actual := LoadShopTransaction(1, trx)
	assert.Equal(t, expected, actual)
}
