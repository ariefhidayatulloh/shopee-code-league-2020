package main

import "time"

func LoadOneHourWindow(trx []Transaction) (result []Transaction) {
	nextHour := trx[0].EventTime.Add(1 * time.Hour)
	for _, val := range trx {
		if val.EventTime.Unix() < nextHour.Unix() {
			result = append(result, val)
		}
	}
	return
}
