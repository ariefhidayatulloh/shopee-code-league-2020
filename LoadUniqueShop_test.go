package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestUniqueShop(t *testing.T) {
	trx := []*Transaction{
		&Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 3,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:01"),
			},
		},
		&Transaction{
			ShopID:  1,
			UserID:  2,
			OrderID: 4,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:02"),
			},
		},
		&Transaction{
			ShopID:  2,
			UserID:  22,
			OrderID: 32,
			EventTime: DateTime{
				parseTime("2019-12-01 00:00:01"),
			},
		},
	}
	expected := []int{1, 2}
	assert.Equal(t, expected, LoadUniqueShop(trx))
}
func parseTime(s string) (t time.Time) {
	t, _ = time.Parse("2006-01-02 15:04:05", s)
	return
}
