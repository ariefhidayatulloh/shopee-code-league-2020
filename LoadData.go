package main

import (
	"log"
	"os"

	"github.com/gocarina/gocsv"
)

func LoadData() (data []*Transaction) {
	clientsFile, err := os.OpenFile("order_brush_order.csv", os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer clientsFile.Close()

	if err := gocsv.UnmarshalFile(clientsFile, &data); err != nil { // Load clients from file
		log.Println(err)
	}
	return
}
